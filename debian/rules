#!/usr/bin/make -f

#export DH_VERBOSE=1
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CPPFLAGS_MAINT_APPEND = -DNDEBUG

%:
	dh $@

override_dh_autoreconf:
	dh_autoreconf ./autogen.sh

override_dh_auto_configure:
	# Remove embedded code copies so we never use them
	rm -rf 3rdparty/miniupnpc
	rm -rf 3rdparty/SDL
	rm -rf 3rdparty/quesoglc
	# Remove generated files so we regenerate them
	rm -f doc/warzone2100.6 \
	src/scriptvals_lexer.cpp \
	src/scriptvals_parser.cpp \
	src/scriptvals_parser.h \
	src/level_lexer.cpp \
	lib/framework/resource_lexer.cpp \
	lib/framework/resource_parser.cpp \
	lib/framework/resource_parser.h \
	lib/script/script_lexer.cpp \
	lib/script/script_parser.cpp \
	lib/script/script_parser.h \
	lib/script/chat_lexer.cpp \
	lib/script/chat_parser.cpp \
	lib/script/chat_parser.h \
	lib/gamelib/audp_lexer.cpp \
	lib/gamelib/audp_parser.cpp \
	lib/gamelib/audp_parser.h \
	lib/framework/strres_lexer.cpp \
	lib/framework/strres_parser.cpp \
	lib/framework/strres_parser.h
	dh_auto_configure -- \
		--bindir=\$${prefix}/games \
		--datadir=\$${prefix}/share/games \
		--with-icondir=\$${prefix}/share/icons/hicolor/128x128/apps \
		--with-applicationdir=\$${prefix}/share/applications \
		--with-distributor="$(shell dpkg-vendor --query vendor)" \
		--with-appdatadir=\$${prefix}/share/appdata \
		--enable-debug=no

override_dh_auto_build:
	make -C lib/framework strres_parser.h
	make -C lib/gamelib audp_parser.h
	make -C lib/script chat_parser.h
	make -C lib/script script_parser.h
	make -C lib/framework resource_parser.h
	make -C src scriptvals_parser.h
	# The HTML to PDF converter needs an X server
	# The default bit depth is too low for the images in the PDF
	xvfb-run --auto-servernum --server-args="-screen 0 1024x768x24" dh_auto_build
	icotool -x icons/warzone2100.ico
	rm -f warzone2100_*_*x*x*.png

override_dh_auto_install:
	dh_auto_install
	rm -f debian/*/usr/share/doc/warzone2100/COPYING*
	rm -f debian/*/usr/share/doc/warzone2100/ChangeLog*
	rm -f debian/*/usr/share/doc/warzone2100/Readme*

override_dh_compress:
	dh_compress -X.pdf

override_dh_auto_test:
	# Do nothing here at the moment to work around a FTBFS

